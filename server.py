#!/usr/bin/python3
from flask import Flask, render_template
import json
import random
w = json.load(open('worldl.json'))
app = Flask(__name__)

@app.route('/continent/<c>')
def continentPage(c):
  return render_template('continent.html',
          l = [i for i in w if i['continent'] == c])

@app.route('/country/<n>')
def countryPage(n):
  return render_template('country.html',
          c = next(i for i in w if i['name']==n)
          )

@app.route('/question/<rnd>')
def question(rnd):
  if rnd == '999':
    rnd = random.randint(0,len(w)-1)
  q = {"lead":"Select the flag for %s." % w[int(rnd)]["name"]}
  ds = set([int(rnd)])
  while len(ds) < 5:
    ds.add(random.randint(0,len(w)-1))
  q["distractors"] = [w[c]["flag"] for c in sorted(list(ds))]
  q["answer"] = w[int(rnd)]["flag"]
  return json.dumps(q)

@app.route('/')
def indexPage():
  return render_template('index.html',
          continentList = sorted(set([c['continent'] for c in w])))

if __name__=='__main__':
  app.run(host='0.0.0.0',port=5036,debug=True)
