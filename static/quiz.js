$(function(){
  $('<button/>',{
    id:'newGame',
    text:'New Game',
    disabled:'disabled',
    click:function(){
      $.ajax({url:'/question/999',dataType:'json',success:function(q){
        let state = {round:1,question:q,players:{}};
        state.players[me] = 0;
        mqtt.publish(topic,JSON.stringify(state));
      }});
    }})
    .appendTo('body');
})
function presentQuestion(state){
  let q = state.question;
  $('#header').text('Round: '+state.round + ' Score: ' + state.players[me]);
  $('#lead').text(q.lead);
  $('#distractors').empty()
  for(let d of q.distractors){
    $('#distractors').append(
     $('<div/>',{data:{correct:q.answer === d},
       click:function(){
         if ($(this).data('correct')){
           state.players[me]++;
         }else{
           state.players[me]--;
         }
         $.ajax({
           url:'/question/999',
           dataType:'json',
           success:function(d){
             state.question = d;
             state.round++;
             mqtt.publish(topic,JSON.stringify(state));
           }
         });
     }}).append($('<img>',{src:'imgs/' + d}))
    );
  }
}
var topic = 'awh';
var round = 0;
var me = 'P'+Math.floor(1000*Math.random());
var mqtt = new Paho.MQTT.Client('broker.hivemq.com',8000,'cid'+Math.random());
mqtt.onMessageArrived = function(d){
  console.log('message arrived',d);
  let rstate = JSON.parse(d.payloadString);
  if (rstate.players[me] === undefined){
    if (Object.keys(rstate.players).length >= 2){
      alert("Game is full, you cannot play");
      return;
    }else{
      console.log("Joining game");
      rstate.players[me] = 0;
      mqtt.publish(topic,JSON.stringify(rstate));
    }
  }else{
    if (Object.keys(rstate.players).length == 2){
       //Two players - I am one of them - let's go!
       if (rstate.round>round){
         round = rstate.round;
         presentQuestion(rstate);
       }else{
         //Someone is trying to generate the same round
         //Ignore it - take the first question only
       }
    }
  }
}
mqtt.connect({
  onSuccess:function(){
    console.log('connected to MQTT broker');
    mqtt.subscribe(topic);
    $('#newGame').attr('disabled',false);
  },
  onFailure:function(d){
    console.log('connection failure',d);
  }
});
